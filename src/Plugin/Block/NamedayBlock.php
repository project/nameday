<?php

/**
 * @file
 * Contains \Drupal\nameday\Plugin\Block\NamedayBlock.
 */

namespace Drupal\nameday\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Provides a 'NamedayBlock' block.
 *
 * @Block(
 *  id = "nameday_block",
 *  admin_label = @Translation("Name day"),
 * )
 */
class NamedayBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $data = _nameday_get_row();

    if (!empty($data)) {
      $build = [
        '#theme'   => 'nameday',
        '#name'    => $data['name'],
        '#holiday' => $data['holiday'],
      ];
    }
    $this->setCacheMetadataForRenderArray($build);

    return $build;
  }

  public function setCacheMetadataForRenderArray(&$build) {
    $cacheMetadata = new BubbleableMetadata();
    $config = \Drupal::config('nameday.settings');
    $cachePrecision = $config->get('cache_precision');

    $cacheMaxAge = 0;

    switch ($cachePrecision) {
      case 'd':
        $cacheMaxAge = strtotime('tomorrow') - time();
        break;

      case 'h':
        $start = date("H:i:s");
        $end = date("H:00:00",strtotime("$start +1 hour"));
        $cacheMaxAge = (strtotime($end) - strtotime($start));
        break;

      case 'm':
        $start = date("H:i:s");
        $end = date("H:i:00", strtotime("$start +1 minute"));
        $cacheMaxAge = (strtotime($end) - strtotime($start));
        break;
    }

    $cacheMetadata->setCacheMaxAge($cacheMaxAge);
    $cacheMetadata->setCacheContexts($config->getCacheContexts());
    $cacheMetadata->setCacheTags($config->getCacheTags());

    $cacheMetadata->applyTo($build);

    return $build;
  }

}
