<?php

/**
 * @file
 * Contains \Drupal\nameday\Form\NamedaySettingsForm.
 */

namespace Drupal\nameday\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configure nameday module.
 */
class NamedaySettingsForm extends ConfigFormBase {

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritDoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nameday_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['nameday.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('nameday.settings');

    $form['show_date'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Show date in the name day'),
      '#default_value' => $config->get('show_date'),
      '#description'   => $this->t('If this checked, the date is displayed before the name days and holidays.'),
    ];

    $form['show_holiday'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Show holiday in the name day, if any'),
      '#default_value' => $config->get('show_holiday'),
    ];

    $options = [];
    $dateTimeFormats = $this->entityTypeManager->getStorage('date_format')->loadMultiple();
    foreach ($dateTimeFormats as $dateTimeFormatId => $dateTimeFormat) {
      $options[$dateTimeFormatId] = $dateTimeFormat->label();
    }
    $options['custom'] = $this->t('Custom date format');

    $form['date_format'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Date format'),
      '#default_value' => $config->get('date_format'),
      '#options'       => $options,
      '#description'   => $this->t('If you want to show the date, You can choose the format of it. You can set it up in the <a href="@url">Date and time settings</a>', [
        '@url' => Url::fromUri('internal:/admin/config/regional/date-time')
          ->toString(),
      ]),
    ];

    $form['date_format_custom'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Custom date format'),
      '#default_value' => $config->get('date_format_custom'),
      '#description'   => $this->t('If You choose the custom date format here You can define the date format. See the <a href="@url">PHP manual</a> for available options.', [
        '@url' => 'http://php.net/manual/function.date.php',
      ]),
      '#states'        => [
        'visible' => [
          ':input[name="date_format"]' => [
            'value' => 'custom',
          ],
        ],
      ],
    ];

    $form['cache_precision'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Cache precision'),
      '#description'   => $this->t('By default, name day blocks are not cached. The blocks are re-rendered every time a page is loaded by the visitors. This makes it possible to show the current date and time even in a millisecond precision for every visitor. This flexibility can cause performance issues on high traffic websites. Here you can set the time that for how long the block should be cached. For example the "Day" option means it caches the result until the next day comes, in other words until midnight. It can be used when you\'re displaying the date without hours or minutes. The same goes for other options. Tune the caching according to the datetime format you\'re using.'),
      '#default_value' => $config->get('cache_precision') ?: '0',
      '#options'       => [
        '0'  => $this->t('Skip caching'),
        'd' => $this->t('Day'),
        'h'   => $this->t('Hour'),
        'm' => $this->t('Minute'),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('nameday.settings');

    $form_state->cleanValues();

    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
